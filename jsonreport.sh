#!/bin/bash

#start
echo '{'
echo '"icon": "fa fa-cog",'  # icon, optional. See http://fortawesome.github.io/Font-Awesome/icons/

# start subgroups
echo '"subgroups": ['
echo '{'

# start subgroup1
echo '"title": "App",'
echo '"icon": "fa fa-cog",'

#start details subgroup1
echo '"content": ['

#state
echo '{"type": "state", "text" : "UP", "value" : "success"},'

#urls
echo '{"type":"url", "text": "url1", "value": "http://www.google.fr", "endline" : "-"},'
echo '{"type":"url", "text": "url2", "value": "http://www.free.fr", "endline" : ""},'

#plain
echo '{"type":"plain", "icon": "fa fa-cog", "text" : "Version1", "value" : "1.0.0", "endline" : "<br/>"},'  #icon optional
echo '{"type":"plain", "text" : "Version2", "value" : "2.0.0", "endline" : ""},'

#others
echo '{"type":"plain", "icon": "fa fa-cog", "text" : "FS", "value" : "80%", "endline" : "<br/>"},'  #icon optional
echo '{"type":"plain", "text" : "FS2", "value" : "50%", "endline" : ""}'

echo ']'  #end details subgroup1

# End subgroup 1
echo '}'

# Start subgroup2
echo ',{'

echo '"title": "Hardware",'

#start details subgroup2
echo '"content": ['
#icon optional
echo '{"type": "plain", "icon": "fa fa-cog", "text" : "loadAverage", "value" : "average:0.1", "endline" : "<br/>"},'
echo '{"type": "plain", "text" : "DiskUsage", "value" : "63%", "endline" : ""}'
echo ']'  # end content

echo '}]'  # end subgroup2

echo '}'   # end
